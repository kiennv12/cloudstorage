package com.udacity.jwdnd.course1.cloudstorage;

import java.io.File;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import com.udacity.jwdnd.course1.cloudstorage.dto.CredentialDto;
import com.udacity.jwdnd.course1.cloudstorage.services.CredentialService;
import com.udacity.jwdnd.course1.cloudstorage.services.EncryptionService;
import com.udacity.jwdnd.course1.cloudstorage.services.UserService;

import io.github.bonigarcia.wdm.WebDriverManager;
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CloudStorageApplicationTests {

	@LocalServerPort
	private int port;

	private WebDriver driver;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private CredentialService credentialService;

	@BeforeAll
	static void beforeAll() {
		WebDriverManager.chromedriver().setup();
	}

	@BeforeEach
	public void beforeEach() {
		this.driver = new ChromeDriver();
	}

	@AfterEach
	public void afterEach() {
		if (this.driver != null) {
			driver.quit();
		}
	}

	@Test
	public void getLoginPage() {
		driver.get("http://localhost:" + this.port + "/login");
		Assertions.assertEquals("Login", driver.getTitle());
	}
	
	// Verify an unauthorized user can only access the login and signup page
	@Test
	public void testUnauthorize() {
		driver.get("http://localhost:" + this.port + "/login");
		Assertions.assertEquals("Login", driver.getTitle());
		
		driver.get("http://localhost:" + this.port + "/signup");
		Assertions.assertEquals("Sign Up", driver.getTitle());
		
		driver.get("http://localhost:" + this.port + "/home");
		Assertions.assertEquals("Login", driver.getTitle());
	}
	
	// Write a test that signs up a new user, login,verifies home page is accessible, logs out, and verifies home page is no longer accessible
	@Test
	public void testFlow() {

		WebDriverWait webDriverWait = new WebDriverWait(driver, 2);

		doMockSignUp("Flow","Test","FT","123");
		doLogIn("FT", "123");

		driver.get("http://localhost:" + this.port + "/home");
		Assertions.assertEquals("Home", driver.getTitle());

		WebElement buttonLogout = driver.findElement(By.id("buttonLogout"));
		buttonLogout.click();

		webDriverWait.until(ExpectedConditions.titleContains("Login"));
		driver.get("http://localhost:" + this.port + "/home");
		Assertions.assertEquals("Login", driver.getTitle());

	}
	
	/**
	 * PLEASE DO NOT DELETE THIS method.
	 * Helper method for Udacity-supplied sanity checks.
	 **/
	private void doMockSignUp(String firstName, String lastName, String userName, String password){
		// Create a dummy account for logging in later.

		// Visit the sign-up page.
		WebDriverWait webDriverWait = new WebDriverWait(driver, 2);
		driver.get("http://localhost:" + this.port + "/signup");
		webDriverWait.until(ExpectedConditions.titleContains("Sign Up"));
		
		// Fill out credentials
		webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("inputFirstName")));
		WebElement inputFirstName = driver.findElement(By.id("inputFirstName"));
		inputFirstName.click();
		inputFirstName.sendKeys(firstName);

		webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("inputLastName")));
		WebElement inputLastName = driver.findElement(By.id("inputLastName"));
		inputLastName.click();
		inputLastName.sendKeys(lastName);

		webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("inputUsername")));
		WebElement inputUsername = driver.findElement(By.id("inputUsername"));
		inputUsername.click();
		inputUsername.sendKeys(userName);

		webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("inputPassword")));
		WebElement inputPassword = driver.findElement(By.id("inputPassword"));
		inputPassword.click();
		inputPassword.sendKeys(password);

		// Attempt to sign up.
		webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("buttonSignUp")));
		WebElement buttonSignUp = driver.findElement(By.id("buttonSignUp"));
		buttonSignUp.click();

		/* Check that the sign up was successful. 
		// You may have to modify the element "success-msg" and the sign-up 
		// success message below depening on the rest of your code.
		*/
		
		webDriverWait.until(ExpectedConditions.titleContains("Login"));
		Assertions.assertTrue(driver.findElement(By.id("success-msg")).getText().contains("You successfully signed up!"));
	}

	public void createNote(String title, String description) {
		WebDriverWait webDriverWait = new WebDriverWait(driver, 2);
		
		webDriverWait.until(ExpectedConditions.titleContains("Home"));
		WebElement noteTab = driver.findElement(By.id("nav-notes-tab"));
		noteTab.click();
		
		webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("addNewNote")));
		WebElement addNewNote = driver.findElement(By.id("addNewNote"));
		addNewNote.click();
		
		webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("note-title")));
		WebElement inputTitle = driver.findElement(By.id("note-title"));
		inputTitle.click();
		inputTitle.sendKeys(title);
		
		webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("note-description")));
		WebElement noteDescription = driver.findElement(By.id("note-description"));
		noteDescription.click();
		noteDescription.sendKeys(description);
		
		WebElement noteSubmit = driver.findElement(By.xpath("//button[text()='Save changes']"));
		noteSubmit.click();
		
		webDriverWait.until(ExpectedConditions.titleContains("Result"));
		WebElement linkHome = driver.findElement(By.xpath("//a[text()='here']"));
		linkHome.click();
	}
	
	public void createCredential(String url, String username, String password) {
		WebDriverWait webDriverWait = new WebDriverWait(driver, 2);
		
		webDriverWait.until(ExpectedConditions.titleContains("Home"));
		WebElement credentialtab = driver.findElement(By.id("nav-credentials-tab"));
		credentialtab.click();
		
		webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("addNewCredential")));
		WebElement addNewCredential = driver.findElement(By.id("addNewCredential"));
		addNewCredential.click();
		
		webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("credential-url")));
		WebElement credentialUrl = driver.findElement(By.id("credential-url"));
		credentialUrl.click();
		credentialUrl.sendKeys(url);
		
		webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("credential-username")));
		WebElement credentialUsername = driver.findElement(By.id("credential-username"));
		credentialUsername.click();
		credentialUsername.sendKeys(username);
		
		webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("credential-password")));
		WebElement credentialPassword = driver.findElement(By.id("credential-password"));
		credentialPassword.click();
		credentialPassword.sendKeys(password);
		
		WebElement credentialSubmit = driver.findElement(By.id("credentialSubmitButton"));
		credentialSubmit.click();
		
		webDriverWait.until(ExpectedConditions.titleContains("Result"));
		WebElement linkHome = driver.findElement(By.xpath("//a[text()='here']"));
		linkHome.click();
	}
	
	/**
	 * PLEASE DO NOT DELETE THIS method.
	 * Helper method for Udacity-supplied sanity checks.
	 **/
	private void doLogIn(String userName, String password)
	{
		// Log in to our dummy account.
		driver.get("http://localhost:" + this.port + "/login");
		WebDriverWait webDriverWait = new WebDriverWait(driver, 2);

		webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("inputUsername")));
		WebElement loginUserName = driver.findElement(By.id("inputUsername"));
		loginUserName.click();
		loginUserName.sendKeys(userName);

		webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("inputPassword")));
		WebElement loginPassword = driver.findElement(By.id("inputPassword"));
		loginPassword.click();
		loginPassword.sendKeys(password);

		webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("login-button")));
		WebElement loginButton = driver.findElement(By.id("login-button"));
		loginButton.click();

		webDriverWait.until(ExpectedConditions.titleContains("Home"));

	}

	/**
	 * PLEASE DO NOT DELETE THIS TEST. You may modify this test to work with the 
	 * rest of your code. 
	 * This test is provided by Udacity to perform some basic sanity testing of 
	 * your code to ensure that it meets certain rubric criteria. 
	 * 
	 * If this test is failing, please ensure that you are handling redirecting users 
	 * back to the login page after a succesful sign up.
	 * Read more about the requirement in the rubric: 
	 * https://review.udacity.com/#!/rubrics/2724/view 
	 */
	@Test
	public void testRedirection() {
		// Create a test account
		doMockSignUp("Redirection","Test","RT","123");
		
		// Check if we have been redirected to the log in page.
		Assertions.assertEquals("http://localhost:" + this.port + "/login", driver.getCurrentUrl());
	}

	/**
	 * PLEASE DO NOT DELETE THIS TEST. You may modify this test to work with the 
	 * rest of your code. 
	 * This test is provided by Udacity to perform some basic sanity testing of 
	 * your code to ensure that it meets certain rubric criteria. 
	 * 
	 * If this test is failing, please ensure that you are handling bad URLs 
	 * gracefully, for example with a custom error page.
	 * 
	 * Read more about custom error pages at: 
	 * https://attacomsian.com/blog/spring-boot-custom-error-page#displaying-custom-error-page
	 */
	@Test
	public void testBadUrl() {
		// Create a test account
		doMockSignUp("URL","Test","UT","123");
		doLogIn("UT", "123");
		
		// Try to access a random made-up URL.
		driver.get("http://localhost:" + this.port + "/some-random-page");
		Assertions.assertFalse(driver.getPageSource().contains("Whitelabel Error Page"));
	}


	/**
	 * PLEASE DO NOT DELETE THIS TEST. You may modify this test to work with the 
	 * rest of your code. 
	 * This test is provided by Udacity to perform some basic sanity testing of 
	 * your code to ensure that it meets certain rubric criteria. 
	 * 
	 * If this test is failing, please ensure that you are handling uploading large files (>1MB),
	 * gracefully in your code. 
	 * 
	 * Read more about file size limits here: 
	 * https://spring.io/guides/gs/uploading-files/ under the "Tuning File Upload Limits" section.
	 */
	@Test
	public void testLargeUpload() {
		// Create a test account
		doMockSignUp("Large File","Test","LFT","123");
		doLogIn("LFT", "123");

		// Try to upload an arbitrary large file
		WebDriverWait webDriverWait = new WebDriverWait(driver, 2);
		String fileName = "upload5m.zip";

		webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("fileUpload")));
		WebElement fileSelectButton = driver.findElement(By.id("fileUpload"));
		fileSelectButton.sendKeys(new File(fileName).getAbsolutePath());

		WebElement uploadButton = driver.findElement(By.id("uploadButton"));
		uploadButton.click();
		try {
			webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("success")));
		} catch (org.openqa.selenium.TimeoutException e) {
			System.out.println("Large File upload failed");
		}
		Assertions.assertFalse(driver.getPageSource().contains("HTTP Status 403 – Forbidden"));

	}
	
	/**
	 * Test create note and display created record on home page
	 */
	@Test
	public void testCreateNote() {

		WebDriverWait webDriverWait = new WebDriverWait(driver, 2);

		doMockSignUp("CreateNote","Test","CNT","123");
		doLogIn("CNT", "123");

		createNote("TestNote", "TestNoteDescription");

		webDriverWait.until(ExpectedConditions.titleContains("Home"));
		Assertions.assertTrue(driver.getPageSource().contains("TestNote"));
		Assertions.assertTrue(driver.getPageSource().contains("TestNoteDescription"));

	}

	/**
	 * Test edit note and display created record on home page
	 */
	@Test
	public void testEditNote() {

		WebDriverWait webDriverWait = new WebDriverWait(driver, 2);

		doMockSignUp("EditNote","Test","ENT","123");
		doLogIn("ENT", "123");

		createNote("TestNote", "TestNoteDescription");

		webDriverWait.until(ExpectedConditions.titleContains("Home"));
		WebElement editButton = driver.findElement(By.xpath("//button[text()='Edit']"));
		editButton.click();
		
		webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("note-title")));
		WebElement inputTitle = driver.findElement(By.id("note-title"));
		inputTitle.click();
		inputTitle.sendKeys("EditedNote");
		
		webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("note-description")));
		WebElement noteDescription = driver.findElement(By.id("note-description"));
		noteDescription.click();
		noteDescription.sendKeys("EditedNoteDescription");
		
		WebElement noteSubmit = driver.findElement(By.xpath("//button[text()='Save changes']"));
		noteSubmit.click();
		
		webDriverWait.until(ExpectedConditions.titleContains("Result"));
		WebElement linkHome = driver.findElement(By.xpath("//a[text()='here']"));
		linkHome.click();

		webDriverWait.until(ExpectedConditions.titleContains("Home"));
		Assertions.assertTrue(driver.getPageSource().contains("EditedNote"));
		Assertions.assertTrue(driver.getPageSource().contains("EditedNoteDescription"));
		
	}
	
	/**
	 * Test edit note and display created record on home page
	 */
	@Test
	public void testDeleteNote() {

		WebDriverWait webDriverWait = new WebDriverWait(driver, 2);

		doMockSignUp("DeleteNote","Test","DNT","123");
		doLogIn("DNT", "123");

		createNote("TestNote", "TestNoteDescription");

		webDriverWait.until(ExpectedConditions.titleContains("Home"));
		WebElement deleteButton = driver.findElement(By.xpath("//a[text()='Delete']"));
		deleteButton.click();
		
		webDriverWait.until(ExpectedConditions.titleContains("Result"));
		WebElement linkHome = driver.findElement(By.xpath("//a[text()='here']"));
		linkHome.click();

		webDriverWait.until(ExpectedConditions.titleContains("Home"));
		Assertions.assertFalse(driver.getPageSource().contains("EditedNote"));
		Assertions.assertFalse(driver.getPageSource().contains("EditedNoteDescription"));
		
	}
	
	/**
	 * Test create credential and display
	 */
	@Test
	public void testCreateCredential() {

		WebDriverWait webDriverWait = new WebDriverWait(driver, 2);

		doMockSignUp("CreateCredential","Test","CCT","123");
		doLogIn("CCT", "123");

		createCredential("https://localhost:8081/", "TestUsername", "testPassword");

		webDriverWait.until(ExpectedConditions.titleContains("Home"));
		
		List<CredentialDto> credentialDtoList = credentialService.getCredentialDtoList(userService.getUser("CCT").getUserId());
		
		String tbody = driver.findElement(By.id("tbody")).getText();
		Assertions.assertTrue(tbody.contains("https://localhost:8081"));
		Assertions.assertTrue(tbody.contains("TestUsername"));
		Assertions.assertFalse(tbody.contains(credentialDtoList.get(0).getDecryptedPassword()));
		Assertions.assertTrue(tbody.contains(credentialDtoList.get(0).getEncryptedPassword()));
	}
	
	/**
	 * Test edit credential and change
	 */
	@Test
	public void testEditCredential() {

		WebDriverWait webDriverWait = new WebDriverWait(driver, 2);

		doMockSignUp("EditCredential","Test","ECT","123");
		doLogIn("ECT", "123");

		createCredential("https://localhost:8081/", "TestUsername", "testPassword");

		webDriverWait.until(ExpectedConditions.titleContains("Home"));
		
		WebElement editButton = driver.findElement(By.xpath("//button[text()='Edit']"));
		editButton.click();
		
		webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("credential-password")));
		WebElement credentialPassword = driver.findElement(By.id("credential-password"));
		Assertions.assertEquals("testPassword", credentialPassword.getAttribute("value"));
		
		credentialPassword.click();
		credentialPassword.sendKeys("newTestPassword");
		
		List<CredentialDto> credentialDtoList = credentialService.getCredentialDtoList(userService.getUser("ECT").getUserId());
		
		String tbody = driver.findElement(By.id("tbody")).getText();
		Assertions.assertTrue(tbody.contains("https://localhost:8081"));
		Assertions.assertTrue(tbody.contains("TestUsername"));
		Assertions.assertEquals("testPassword", credentialDtoList.get(0).getDecryptedPassword());
		Assertions.assertFalse(tbody.contains(credentialDtoList.get(0).getDecryptedPassword()));
		Assertions.assertTrue(tbody.contains(credentialDtoList.get(0).getEncryptedPassword()));
	}
	
	/**
	 * Test delete credential and change display
	 */
	@Test
	public void testDeleteCredential() {

		WebDriverWait webDriverWait = new WebDriverWait(driver, 2);

		doMockSignUp("DeleteCredential","Test","DCT","123");
		doLogIn("DCT", "123");

		createCredential("https://localhost:8081/", "TestUsername", "testPassword");

		webDriverWait.until(ExpectedConditions.titleContains("Home"));
		
		WebElement deleteButton = driver.findElement(By.xpath("//a[text()='Delete']"));
		deleteButton.click();
		
		webDriverWait.until(ExpectedConditions.titleContains("Result"));
		WebElement linkHome = driver.findElement(By.xpath("//a[text()='here']"));
		linkHome.click();
		
		List<CredentialDto> credentialDtoList = credentialService.getCredentialDtoList(userService.getUser("DCT").getUserId());
		
		String tbody = driver.findElement(By.id("tbody")).getText();
		Assertions.assertFalse(tbody.contains("https://localhost:8081"));
		Assertions.assertFalse(tbody.contains("TestUsername"));
		Assertions.assertEquals(0,credentialDtoList.size());

	}
}
