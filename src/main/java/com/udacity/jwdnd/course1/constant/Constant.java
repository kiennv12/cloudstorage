package com.udacity.jwdnd.course1.constant;

public class Constant {
	
	public static final String SIGNUP_ERROR = "There was an error signing you up. Please try again.";
	
	public static final String EXIST_USERNAME_ERROR = "The username already exists.";

	public static final String RECORD_ERROR_SAVE = "Record has not been saved";
	
	public static final String RECORD_ERROR_DELETE = "Record has not been deleted";
	
	public static final String EDIT_ERROR = "Your change has not been saved";
	
	public static final String MSG_SUCCESS = "msgSuccess";
	
	public static final String MSG_ERROR = "msgError";
	
	public static final String URL_ERROR = "URL is not valid";
	
	public static final String FILE_EXIST_ERROR = "File name is already existed";
}
