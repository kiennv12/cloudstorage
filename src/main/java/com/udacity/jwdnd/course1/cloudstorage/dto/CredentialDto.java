package com.udacity.jwdnd.course1.cloudstorage.dto;

public class CredentialDto {

	private Integer credentialId;
	private String url;
	private String username;
	private String encryptedPassword;
	private String decryptedPassword;
	
	// No argument constructor
	public CredentialDto() {
	}

	// Have arguments constructor
	public CredentialDto(Integer credentialId, String url, String username, String encryptedPassword,
			String decryptedPassword) {
		this.credentialId = credentialId;
		this.url = url;
		this.username = username;
		this.encryptedPassword = encryptedPassword;
		this.decryptedPassword = decryptedPassword;
	}

	public Integer getCredentialId() {
		return credentialId;
	}

	public void setCredentialId(Integer credentialId) {
		this.credentialId = credentialId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEncryptedPassword() {
		return encryptedPassword;
	}

	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}

	public String getDecryptedPassword() {
		return decryptedPassword;
	}

	public void setDecryptedPassword(String decryptedPassword) {
		this.decryptedPassword = decryptedPassword;
	}
}
