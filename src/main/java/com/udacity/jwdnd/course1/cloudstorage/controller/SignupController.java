package com.udacity.jwdnd.course1.cloudstorage.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.udacity.jwdnd.course1.cloudstorage.model.User;
import com.udacity.jwdnd.course1.cloudstorage.services.UserService;
import com.udacity.jwdnd.course1.constant.Constant;

@Controller
public class SignupController {

	@Autowired
	private UserService userService;

	@GetMapping(value = "/signup")
	public String index() {
		return "signup";
	}

	@PostMapping(value = "/signup/save")
	public String signup(@ModelAttribute User user, Model model, RedirectAttributes redirectAttributes) {
		String signupError = null;

		if (!userService.isUsernameAvailable(user.getUsername())) {
			signupError = Constant.EXIST_USERNAME_ERROR;
		}

		if (signupError == null) {
			int rowsAdded = userService.createUser(user);
			if (rowsAdded < 0) {
				signupError = Constant.SIGNUP_ERROR;
			}
		}

		if (signupError == null) {
			redirectAttributes.addFlashAttribute(Constant.MSG_SUCCESS, true);

			return "redirect:/login";
		} else {
			model.addAttribute(Constant.MSG_ERROR, signupError);
		}

		return "signup";
	}
}
