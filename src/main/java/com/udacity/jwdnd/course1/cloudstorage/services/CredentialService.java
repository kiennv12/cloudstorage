package com.udacity.jwdnd.course1.cloudstorage.services;

import java.net.URL;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.udacity.jwdnd.course1.cloudstorage.dto.CredentialDto;
import com.udacity.jwdnd.course1.cloudstorage.mapper.CredentialMapper;
import com.udacity.jwdnd.course1.cloudstorage.model.Credential;

@Service
public class CredentialService {

	@Autowired
	private CredentialMapper credentialMapper;

	@Autowired
	private EncryptionService encryptionService;

	public List<CredentialDto> getCredentialDtoList(Integer userId) {

		List<Credential> credentialList = credentialMapper.getCredentials(userId);

		List<CredentialDto> credentialDtoList = new ArrayList<CredentialDto>();

		for (Credential credential : credentialList) {

			CredentialDto credentialDto = new CredentialDto();

			String decryptedPassword = encryptionService.decryptValue(credential.getPassword(), credential.getKey());

			credentialDto.setCredentialId(credential.getCredentialId());
			credentialDto.setUrl(credential.getUrl());
			credentialDto.setEncryptedPassword(credential.getPassword());
			credentialDto.setDecryptedPassword(decryptedPassword);
			credentialDto.setUsername(credential.getUsername());
			credentialDtoList.add(credentialDto);
		}

		return credentialDtoList;
	}

	public int saveCredential(Credential credential) {
		Credential encryptedCredential = encryptCredentialPassword(credential);
		return credentialMapper.saveCredential(encryptedCredential);
	}

	public int deleteCredential(Integer credentialId) {
		return credentialMapper.deleteCredential(credentialId);
	}

	public int updateCredential(Credential credential) {
		Credential encryptedCredential = encryptCredentialPassword(credential);
		return credentialMapper.updateCredential(encryptedCredential);
	}

	private Credential encryptCredentialPassword(Credential credential) {

		SecureRandom random = new SecureRandom();
		byte[] key = new byte[16];
		random.nextBytes(key);
		String encodedKey = Base64.getEncoder().encodeToString(key);
		String encryptedPassword = encryptionService.encryptValue(credential.getPassword(), encodedKey);
		credential.setPassword(encryptedPassword);
		credential.setKey(encodedKey);

		return credential;
	}
	
    public boolean isValidURL(String url) {
        try {
            new URL(url).toURI();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
