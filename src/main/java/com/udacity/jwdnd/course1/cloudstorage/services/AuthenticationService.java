package com.udacity.jwdnd.course1.cloudstorage.services;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;

import com.udacity.jwdnd.course1.cloudstorage.mapper.UserMapper;
import com.udacity.jwdnd.course1.cloudstorage.model.User;

@Service
public class AuthenticationService implements AuthenticationProvider {
	
	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private HashService hashService;
	
	@Autowired
	private HttpSession session;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String username = authentication.getName();
		String password = authentication.getCredentials().toString();

		User user = userMapper.getUser(username);
		if (user != null) {
			String encodedSalt = user.getSalt();
			String hashedPassword = hashService.getHashedValue(password, encodedSalt);
			if (user.getPassword().equals(hashedPassword)) {
				session.setAttribute("userId", user.getUserId());
				session.setAttribute("currentTab", "1");
				return new UsernamePasswordAuthenticationToken(username, hashedPassword, new ArrayList<>());
			}
		}
		
		return null;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
}
