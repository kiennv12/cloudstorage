package com.udacity.jwdnd.course1.cloudstorage.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.udacity.jwdnd.course1.cloudstorage.dto.NoteDto;
import com.udacity.jwdnd.course1.cloudstorage.mapper.NoteMapper;
import com.udacity.jwdnd.course1.cloudstorage.model.Note;

@Service
public class NoteService {

	@Autowired
	private NoteMapper noteMapper;

	public List<NoteDto> getNoteDtoList(Integer userId) {

		List<Note> noteList = noteMapper.getNote(userId);
		List<NoteDto> noteDtoList = new ArrayList<NoteDto>();

		for (Note note : noteList) {
			NoteDto noteDto = new NoteDto();
			noteDto.setNoteId(note.getNoteId());
			noteDto.setNoteTitle(note.getNoteTitle());
			noteDto.setNoteDescription(note.getNoteDescription());

			noteDtoList.add(noteDto);
		}

		return noteDtoList;
	}

	public int saveNote(Note note) {
		return noteMapper.saveNote(note);
	}

	public int deleteNote(Integer noteId) {
		return noteMapper.deleteNote(noteId);
	}

	public int updateNote(Note note) {
		return noteMapper.updateNote(note);
	}
}
