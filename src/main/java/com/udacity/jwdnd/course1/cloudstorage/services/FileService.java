package com.udacity.jwdnd.course1.cloudstorage.services;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.udacity.jwdnd.course1.cloudstorage.dto.FileDto;
import com.udacity.jwdnd.course1.cloudstorage.mapper.FileMapper;
import com.udacity.jwdnd.course1.cloudstorage.model.File;

@Service
public class FileService {

	@Autowired
	private FileMapper fileMapper;

	public List<FileDto> getFileDtoList(Integer userId) {

		List<File> fileList = fileMapper.getFiles(userId);
		List<FileDto> fileDtoList = new ArrayList<FileDto>();

		for(File file : fileList) {

			FileDto fileDto = new FileDto();
			StringBuilder sb = new StringBuilder();

			String fileDataToString = new String(file.getFileData(), StandardCharsets.UTF_8);
			sb.append("data:").append(file.getContentType()).append(";base64,").append(fileDataToString);
			
			fileDto.setFileId(file.getFileId());
			fileDto.setFileName(file.getFileName());
			fileDto.setBase64FileData(sb.toString());

			fileDtoList.add(fileDto);
		}

		return fileDtoList;
	}

	public int saveFile(MultipartFile multipartFile, Integer userId) throws IOException {

		String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
		String contentType = multipartFile.getContentType();
		String size = String.valueOf(multipartFile.getSize());
		byte[] fileData = Base64.encodeBase64(multipartFile.getBytes());

		return fileMapper.saveFile(new File(null,fileName, contentType, size, userId, fileData));
	}

	public int deleteFile(Integer fileId) {
		return fileMapper.deleteFile(fileId);
	}

	public List<File> getFilesByFileName(String fileName) {
		List<File> fileList = new ArrayList<File>();
		fileList = fileMapper.getFileByFileName(fileName);
		return fileList;
	}
}
