package com.udacity.jwdnd.course1.cloudstorage.dto;

public class FileDto {

	private Integer fileId;
	private String fileName;
	private String base64FileData;

	// No argument constructor
	public FileDto() {
	}

	// Have arguments constructor
	public FileDto(Integer fileId, String fileName, String base64FileData) {
		this.fileId = fileId;
		this.fileName = fileName;
		this.base64FileData = base64FileData;
	}

	// Getter setter
	public Integer getFileId() {
		return fileId;
	}

	public void setFileId(Integer fileId) {
		this.fileId = fileId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getBase64FileData() {
		return base64FileData;
	}

	public void setBase64FileData(String base64FileData) {
		this.base64FileData = base64FileData;
	}
}
