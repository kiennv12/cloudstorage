package com.udacity.jwdnd.course1.cloudstorage.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.udacity.jwdnd.course1.cloudstorage.model.Credential;
import com.udacity.jwdnd.course1.cloudstorage.model.File;
import com.udacity.jwdnd.course1.cloudstorage.model.Note;
import com.udacity.jwdnd.course1.cloudstorage.services.CredentialService;
import com.udacity.jwdnd.course1.cloudstorage.services.FileService;
import com.udacity.jwdnd.course1.cloudstorage.services.NoteService;
import com.udacity.jwdnd.course1.constant.Constant;

@Controller
@RequestMapping("/home")
public class HomeController {

	@Autowired
	private FileService fileService;

	@Autowired
	private NoteService noteService;

	@Autowired
	private CredentialService credentialService;

	@Autowired
	private HttpSession session;

	public Integer userId;

	@GetMapping()
	public String index(Model model) {

		userId = (Integer) session.getAttribute("userId");
		Map<String, Object> bindmap = new HashMap<String, Object>();

		bindmap.put("currentTab", session.getAttribute("currentTab"));
		bindmap.put("fileList", fileService.getFileDtoList(userId));
		bindmap.put("noteList", noteService.getNoteDtoList(userId));
		bindmap.put("credentialDtoList", credentialService.getCredentialDtoList(userId));

		model.addAllAttributes(bindmap);

		return "home";
	}
	
	@PostMapping(value = "/file/save")
	public String saveFile(@RequestParam("file") MultipartFile multipartFile, Model model) {
		
		session.setAttribute("currentTab", "1");

		String msgError = null;

		List<File> filesByFileName = fileService.getFilesByFileName(multipartFile.getOriginalFilename());

		if (filesByFileName.size() > 0) {
			msgError = Constant.FILE_EXIST_ERROR;
		} else {
			try {
				int rowAdded = fileService.saveFile(multipartFile, userId);
				if(rowAdded <= 0) {
					msgError = Constant.RECORD_ERROR_SAVE;
				}
			}catch (IOException e) {
				msgError = Constant.RECORD_ERROR_SAVE;
			}
		}

		if (msgError == null) {
			model.addAttribute(Constant.MSG_SUCCESS, true);
		} else {
			model.addAttribute(Constant.MSG_ERROR, msgError);
		}

		return "/result";
	}
	
	@GetMapping(value = "/file/{fileId}/delete")
	public String deleteFile(@PathVariable("fileId") Integer fileId, Model model) {
		
		session.setAttribute("currentTab", "1");

		String msgError = null;

		int rowDeleted = fileService.deleteFile(fileId);
		if (rowDeleted <= 0) {
			msgError = Constant.RECORD_ERROR_DELETE;
		}

		if (msgError == null) {
			model.addAttribute(Constant.MSG_SUCCESS, true);
		} else {
			model.addAttribute(Constant.MSG_ERROR, msgError);
		}

		return "/result";
	}

	@PostMapping(value = "/note/save")
	public String saveNote(@ModelAttribute Note note, Model model) {
		
		session.setAttribute("currentTab", "2");

		String msgError = null;

		int rowAdded = noteService.saveNote(new Note(null, note.getNoteTitle(), note.getNoteDescription(), userId));
		if (rowAdded <= 0) {
			msgError = Constant.RECORD_ERROR_SAVE;
		}

		if (msgError == null) {
			model.addAttribute(Constant.MSG_SUCCESS, true);
		} else {
			model.addAttribute(Constant.MSG_ERROR, msgError);
		}

		return "/result";
	}

	@GetMapping(value = "/note/{noteId}/delete")
	public String deleteNote(@PathVariable("noteId") Integer noteId, Model model) {
		
		session.setAttribute("currentTab", "2");

		String msgError = null;
		
		int rowDeleted = noteService.deleteNote(noteId);
		if (rowDeleted <= 0) {
			msgError = Constant.RECORD_ERROR_DELETE;
		}

		if (msgError == null) {
			model.addAttribute(Constant.MSG_SUCCESS, true);
		} else {
			model.addAttribute(Constant.MSG_ERROR, msgError);
		}

		return "/result";
	}
	
	@PostMapping(value = "/note/update")
	public String updateNote(@ModelAttribute Note note, Model model) {
		
		session.setAttribute("currentTab", "2");

		String msgError = null;

		int rowUpdated = noteService.updateNote(note);
		if (rowUpdated <= 0) {
			msgError = Constant.EDIT_ERROR;
		}

		if (msgError == null) {
			model.addAttribute(Constant.MSG_SUCCESS, true);
		} else {
			model.addAttribute(Constant.MSG_ERROR, msgError);
		}

		return "/result";
	}
	
	@PostMapping(value = "/credential/save")
	public String saveCredential(@ModelAttribute Credential credential, Model model) {

		session.setAttribute("currentTab", "3");

		String msgError = null;
		
		if (!credentialService.isValidURL(credential.getUrl())) {
			msgError = Constant.URL_ERROR;
		}
		
		if (msgError == null) {
			int rowAdded = credentialService.saveCredential(new Credential(null, credential.getUrl(), credential.getUsername(), credential.getKey(), credential.getPassword(), userId));
			if (rowAdded <= 0) {
				msgError = Constant.EDIT_ERROR;
			}
		}

		if (msgError == null) {
			model.addAttribute(Constant.MSG_SUCCESS, true);
		} else {
			model.addAttribute(Constant.MSG_ERROR, msgError);
		}

		return "/result";
	}

	@GetMapping(value = "/credential/{credentialId}/delete")
	public String deleteCredential(@PathVariable("credentialId") Integer credentialId, Model model) {

		session.setAttribute("currentTab", "3");
		
		String msgError = null;

		int rowDeleted = credentialService.deleteCredential(credentialId);
		if (rowDeleted <= 0) {
			msgError = Constant.RECORD_ERROR_DELETE;
		}

		if (msgError == null) {
			model.addAttribute(Constant.MSG_SUCCESS, true);
		} else {
			model.addAttribute(Constant.MSG_ERROR, msgError);
		}

		return "/result";
	}
	
	@PostMapping(value = "/credential/update")
	public String updateCredential(@ModelAttribute Credential credential, Model model) {

		session.setAttribute("currentTab", "3");

		String msgError = null;

		if (!credentialService.isValidURL(credential.getUrl())) {
			msgError = Constant.URL_ERROR;
		}

		if(msgError == null) {
			int rowUpdated = credentialService.updateCredential(credential);
			if (rowUpdated <= 0) {
				msgError = Constant.EDIT_ERROR;
			}
		}

		if (msgError == null) {
			model.addAttribute(Constant.MSG_SUCCESS, true);
		} else {
			model.addAttribute(Constant.MSG_ERROR, msgError);
		}

		return "/result";
	}
	
	@GetMapping(value = "/result")
	public String result() {
		return "result";
	}
}
